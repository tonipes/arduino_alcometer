#pragma once

#define LCD_COMMAND 0
#define LCD_DATA 1
#define TILE_W 7
#define TILE_H 8

namespace Scr {
	enum TextType {
		STANDARD,
		BIGNUM
	};

	class Screen {
	public:
		int h;
		int w;
		Screen(int pin_ce, int pin_reset, int pin_dc, int pin_din, int pin_clk,
			int lcd_x, int lcd_y);//, char lcd_vop, char lcd_tempCoff, char lcd_bias);

		void Clear(void);

		void WriteText(char const *characters);

		void WriteCharacter(char character);

		void WriteTile(char const (&data)[8]);

		void WriteTiles(char const data[][8]);

		void Write(char data_or_command, char data);

		void WriteArray(char my_array[]);

		void Init(char lcd_vop, char lcd_tempCoff, char lcd_bias);

		void SetCursor(int x, int y);

	private:
		int p_ce;			//Pin 3 on LCD
		int p_reset;	//Pin 4 on LCD
		int p_dc;			//Pin 5 on LCD
		int p_din;		//Pin 6 on LCD
		int p_clk;		//Pin 7 on LCD

		char lcd_vop = 0xBF;
		char lcd_tempCoff = 0x04;
		char lcd_bias = 0x14;
	};
}
