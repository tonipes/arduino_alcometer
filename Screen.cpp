#include<Screen.h>
#include<Arduino.h>
#include<CharLib.h>

using namespace Scr;

Screen::Screen(
	int pin_ce, int pin_reset, int pin_dc, 
	int pin_din, int pin_clk, int lcd_x, int lcd_y 
	)
{
	p_ce = pin_ce; p_reset = pin_reset; p_dc = pin_dc; p_din = pin_din; p_clk = pin_clk;
	w = lcd_x; h = lcd_y;
}

void Screen::SetCursor(int x, int y) {
	Write(LCD_COMMAND, 0x80 | x*TILE_W);
	Write(LCD_COMMAND, 0x40 | y);
}

void Screen::Clear() {
	for (int i = 0; i < (w * h / 8); i++)
		Write(LCD_DATA, 0x00);
	SetCursor(0, 0);
}

void Screen::Init(char lcd_vop, char lcd_tempCoff, char lcd_bias) {
	pinMode(p_ce, OUTPUT);
	pinMode(p_reset, OUTPUT);
	pinMode(p_dc, OUTPUT);
	pinMode(p_din, OUTPUT);
	pinMode(p_clk, OUTPUT);

	digitalWrite(p_reset, LOW);
	digitalWrite(p_reset, HIGH);

	Write(LCD_COMMAND, 0x21);					//Tell LCD that extended commands follow
	Write(LCD_COMMAND, lcd_vop);			//Set LCD Vop (Contrast): Try 0xB1(good @ 3.3V) or 0xBF if your display is too dark
	Write(LCD_COMMAND, lcd_tempCoff); //Set Temp coefficent
	Write(LCD_COMMAND, lcd_bias);			//LCD bias mode 1:48: Try 0x13 or 0x14

	Write(LCD_COMMAND, 0x20);					//We must send 0x20 before modifying the display control mode
	Write(LCD_COMMAND, 0x0C);					//Set display control, normal mode. 0x0D for inverse
}

void Screen::Write(char mode, char data) {
	digitalWrite(p_dc, mode); //Tell the LCD that we are writing either to data or a command

	digitalWrite(p_ce, LOW);
	shiftOut(p_din, p_clk, MSBFIRST, data);
	digitalWrite(p_ce, HIGH);
}

void Screen::WriteTiles(char const data[][8]) {
	int count = sizeof(data) / sizeof(data[0]);
	Serial.println("Writing tiles");
	for (int i; i < count; i++) {
		Serial.println(data[i]);
		WriteTile(data[i]);
	}
}

void Screen::WriteCharacter(char character) {

	Write(LCD_DATA, 0x00); //Blank vertical line padding

	for (int index = 0; index < 5; index++)
		Write(LCD_DATA, ASCII[character - 0x20][index]);

	Write(LCD_DATA, 0x00); //Blank vertical line padding
}

void Screen::WriteTile(char const (&data)[8]) {
	for (int i = 0; i < 7; i++)
		Write(LCD_DATA, data[i]);
}

void Screen::WriteText(char const *characters) {
	while (*characters)
		WriteCharacter(*characters++);
}

void Screen::WriteArray(char my_array[]) {

}